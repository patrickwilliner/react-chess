import { useState } from 'react';
import './App.scss';
import Board from './Board';
import StringUtil from './util/StringUtil';
import RuleUtil, { EMPTY_FIELD, FEN_POS_PLAYER } from './util/RuleUtil';
import InfoPanel from './InfoPanel';
import History from './History';
import NotationUtil from './util/NotationUtil';
import { HistoryItem } from './models';

const INITIAL_FEN =
  'rnbqkbnrppppppppxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxPPPPPPPPRNBQKBNR w KQkq';

function getInitialHistory(): HistoryItem[] {
  return [{ fen: INITIAL_FEN, move: undefined }];
}

export default function App() {
  const [fen, setFen] = useState<string>(INITIAL_FEN);
  const [history, setHistory] = useState<HistoryItem[]>(getInitialHistory());

  const player = RuleUtil.parsePlayer(fen);

  const isValidMove = (from: number, to: number): boolean => {
    return RuleUtil.isValidMove(fen, from, to);
  };

  const onMovePiece = (from: number, to: number): void => {
    if (isValidMove(from, to)) {
      movePiece(from, to);
      createHistoryEntry(from, to);
    }
  };

  const movePiece = (from: number, to: number): void => {
    let newPositions = StringUtil.replaceAt(fen, to, fen[from]);
    newPositions = StringUtil.replaceAt(newPositions, from, EMPTY_FIELD);
    newPositions = StringUtil.replaceAt(
      newPositions,
      FEN_POS_PLAYER,
      player === 'w' ? 'b' : 'w',
    );
    setFen(newPositions);
  };

  const createHistoryEntry = (from: number, to: number): void => {
    const fromPiece = RuleUtil.parsePiece(fen, from).toLowerCase();
    const toPiece = fen[to];
    let move = '';

    if (fromPiece !== 'p') {
      move += fromPiece;
    }

    move += NotationUtil.indexToFieldName(to);

    setHistory([...history, { fen, move }]);
  };

  const undo = () => {
    const previousFen =
      (history.length > 0 && history[history.length - 1].fen) || undefined;
    if (previousFen != null) {
      setFen(previousFen);
      setHistory(history.slice(0, history.length - 1));
    }
  };

  const reset = () => {
    setHistory(getInitialHistory());
    setFen(INITIAL_FEN);
  };

  let historyLogs = history
    .map((h) => h.move)
    .filter((h) => h != null) as string[];

  return (
    <div className="content">
      <Board positions={fen} onMovePiece={onMovePiece}></Board>
      <InfoPanel player={player} undoFn={undo} resetFn={reset}>
        <History items={historyLogs}></History>
      </InfoPanel>
    </div>
  );
}
