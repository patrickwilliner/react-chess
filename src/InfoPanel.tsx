import { ReactNode } from 'react';
import { Player } from './util/RuleUtil';
import './InfoPanel.scss';

export type InfoPanelProperties = {
  player: Player;
  children: ReactNode;
  undoFn: () => void;
  resetFn: () => void;
};

export default function InfoPanel({
  player,
  children,
  undoFn,
  resetFn,
}: InfoPanelProperties) {
  return (
    <div className="panel">
      <div className="player-row">
        <div>Player:</div>
        <div className={`player ${player === 'b' ? 'black' : 'white'}`}>
          &nbsp;
        </div>
      </div>

      <div className="history">{children}</div>

      <div className="actions">
        <button onClick={() => resetFn()}>Reset</button>
        <button onClick={() => undoFn()}>Undo</button>
      </div>
    </div>
  );
}
