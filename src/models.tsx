export type HistoryItem = {
  fen: string;
  move: string | undefined;
};
