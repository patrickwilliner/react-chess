import './History.scss';

export type HistoryProperties = {
  items: string[];
};

export default function History({ items }: HistoryProperties) {
  return (
    <div className="items">
      {items.map((item, i) => (
        <div className="item">
          {i + 1}. {item}
        </div>
      ))}
    </div>
  );
}
