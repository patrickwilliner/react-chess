import { ReactNode } from 'react';
import './Board.scss';

type Properties = {
  positions: string;
  onMovePiece: (from: number, to: number) => void;
};

export default function Board({ positions, onMovePiece }: Properties) {
  return <div>{createBoard(positions, onMovePiece)}</div>;
}

function createBoard(
  positions: string,
  moveCb: (from: number, to: number) => void,
): ReactNode[] {
  let rows: ReactNode[] = [];
  for (let i = 0; i < 8; i++) {
    let cols: ReactNode[] = [];
    cols.push(<div className="label">{8 - i}</div>);

    for (let ii = 0; ii < 8; ii++) {
      let index = i * 8 + ii;
      let piece = positions.at(index);
      let bgColor = (ii + i) % 2 === 0 ? 'light' : 'dark';
      cols.push(
        <div
          className={`field ${bgColor}`}
          onDragEnter={onDragEnter}
          onDragOver={onDragOver}
          onDrop={(e) => onDrop(e, index, moveCb)}
        >
          {piece !== 'x' && (
            <img
              src={`/assets/${piece}.svg`}
              className="cursor-move"
              onDragStart={(e) => onDragStart(e, index)}
            />
          )}
        </div>,
      );
    }
    rows.push(<div className="row">{cols}</div>);
  }

  rows.push(
    <div className="row">
      <div className="label">&nbsp;</div>
      {[...Array(8)].map((_, i) => (
        <div className="label">
          {String.fromCharCode('A'.charCodeAt(0) + i)}
        </div>
      ))}
    </div>,
  );

  return rows;
}

function onDragStart(e: any, fieldIndex: number): void {
  e.dataTransfer.setData('field', fieldIndex);
}

function onDragEnter(e: any): void {
  e.preventDefault();
}

function onDragOver(e: any): void {
  e.preventDefault();
}

function onDrop(
  e: any,
  fieldIndex: number,
  moveCb: (from: number, to: number) => void,
): void {
  let sourceFieldIndex = parseInt(e.dataTransfer.getData('field'));
  moveCb(sourceFieldIndex, fieldIndex);
}
