import StringUtil from './StringUtil';

test('renders learn react link', () => {
  expect(StringUtil.replaceAt('abcd', 1, 'B')).toBe('aBcd');
  expect(StringUtil.replaceAt('abcd', 1, 'BB')).toBe('aBBcd');
});
