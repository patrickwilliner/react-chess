import NotationUtil from './NotationUtil';

test('maps index to field name', () => {
  expect(NotationUtil.indexToFieldName(0)).toBe('a8');
  expect(NotationUtil.indexToFieldName(5)).toBe('f8');
  expect(NotationUtil.indexToFieldName(8)).toBe('a7');
});
