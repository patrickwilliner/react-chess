import StringUtil from './StringUtil';

export type Player = 'w' | 'b';
export type UncoloredPiece = 'r' | 'n' | 'b' | 'q' | 'k' | 'p';
export type WhitePiece = Uppercase<UncoloredPiece>;
export type BlackPiece = Lowercase<UncoloredPiece>;
export type Piece = WhitePiece | BlackPiece;

export const EMPTY_FIELD = 'x';
export const FEN_POS_PLAYER = 65;

const REGEX_PIECE = /r|n|b|q|k|p/i;
const REGEX_PLAYER = /w|b/;

export default class RuleUtil {
  constructor() {
    throw Error();
  }

  public static isValidMove(fen: string, from: number, to: number): boolean {
    if (from === to) {
      return false;
    } else {
      const player = this.parsePlayer(fen);
      const piece = this.parsePiece(fen, from);
      const diff = to - from;
      const absDiff = Math.abs(from - to);

      if (!this.isRightColorMove(player, piece)) {
        return false;
      }

      switch (piece) {
        case 'p':
          return (
            diff === 8 ||
            (Math.floor(from / 8) === 1 && diff === 16) ||
            ((diff === 9 || diff === 7) &&
              fen[to] !== EMPTY_FIELD &&
              !StringUtil.isLowerCase(fen[to]))
          );

        case 'P':
          return (
            diff === -8 ||
            (Math.floor(from / 8) === 6 && diff === -16) ||
            ((diff === -9 || diff === -7) &&
              fen[to] !== EMPTY_FIELD &&
              StringUtil.isLowerCase(fen[to]))
          );

        case 'r':
        case 'R':
          return (
            Math.floor(from / 8) === Math.floor(to / 8) || (to - from) % 8 === 0
          );

        case 'n':
        case 'N':
          return (
            absDiff === 6 || absDiff === 10 || absDiff === 15 || absDiff === 17
          );

        case 'b':
        case 'B':
          return absDiff % 9 === 0 || absDiff % 7 === 0;

        case 'q':
        case 'Q':
          return (
            Math.floor(from / 8) === Math.floor(to / 8) ||
            (to - from) % 8 === 0 ||
            absDiff % 9 === 0 ||
            absDiff % 7 === 0
          );

        case 'k':
        case 'K':
          return (
            absDiff === 1 || absDiff === 7 || absDiff === 8 || absDiff === 9
          );

        default:
          return false;
      }
    }
  }

  public static parsePlayer(fen: string): Player {
    const player = fen[FEN_POS_PLAYER];
    if (player.match(REGEX_PLAYER)) {
      return player as Player;
    } else {
      throw Error(`Illegal FEN format: ${fen}`);
    }
  }

  public static parsePiece(fen: string, position: number): Piece {
    const piece = fen[position];
    if (piece.match(REGEX_PIECE)) {
      return piece as Piece;
    } else {
      throw Error(`Illegal FEN format: ${fen}`);
    }
  }

  private static isRightColorMove(player: Player, piece: Piece): boolean {
    return (
      (player === 'w' && !StringUtil.isLowerCase(piece)) ||
      (player === 'b' && StringUtil.isLowerCase(piece))
    );
  }

  private static isBeatingMove(
    positions: string,
    from: number,
    to: number,
  ): boolean {
    return false;
  }
}
