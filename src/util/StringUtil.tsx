export default class StringUtil {
  private constructor() {
    throw Error();
  }

  static replaceAt(str: string, index: number, replacement: string): string {
    return (
      str.substring(0, index) +
      replacement +
      str.substring(index + replacement.length)
    );
  }

  static isLowerCase(str: string): boolean {
    return str !== null && str !== '' && str.toLowerCase() === str;
  }
}
