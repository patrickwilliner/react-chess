import { Piece } from './RuleUtil';

export default class NotationUtil {
  constructor() {
    throw new Error();
  }

  public static toHistoryNotation(
    from: number,
    to: number,
    fromPiece: Piece,
    toPiece: Piece,
  ): string {
    return 'xxx';
  }

  public static indexToFieldName(index: number): string {
    const column = String.fromCharCode('a'.charCodeAt(0) + (index % 8));
    const row = 8 - Math.floor(index / 8);
    return column + row;
  }
}
